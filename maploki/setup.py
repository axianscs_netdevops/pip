import setuptools

setuptools.setup(
    name="maploki",
    version="0.1",
    author="k0rventen",
    description="A simple grafana - loki logger lib",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License"
        "Operating System :: OS Independent",
    ],
    install_requires=['requests',"maplog"],
)