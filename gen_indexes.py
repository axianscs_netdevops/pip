import re
import os

from mako.template import Template
import setuptools

index_template = Template("""
<html>
<head><title>${project} pip index</title></head>
<body>
<h1>${project}</h1>
    % for package_path, package_name in packages:
        <a href="./${package_path}">${package_name}</a><br/>
    % endfor
</body>
</html>
""")

package_template = Template("""
<html>
<head><title>${project} pip index - ${package}</title></head>
<body>
<h1>${package}</h1>
    <ul>
    % for name, url in package_files:
        <li><a href="${url}">${name}</a><br/></li>
    % endfor
    </ul>
</body>
</html>
""")

def list_packages(path):
    subs = [ (f,f) for f in  next(os.walk(path))[1]]
    return subs

def list_wheels(package_dir):
    files = [ (f,"./"+f) for f in next(os.walk(package_dir))[2]]
    return files

def gen_package_index(package_dir):
    wheels = list_wheels(package_dir)
    with open(package_dir+"/index.html",'w+') as fd:
        package = package_template.render(project='Axians',package='lol',package_files=wheels,)
        fd.write(package)
        
def gen_main_index():
    folders = list_packages("./public/")
    with open("public/index.html",'w+') as fd:
        packageContent = index_template.render(project="Axians",packages=folders)
        fd.write(packageContent)


if __name__ == "__main__":
    gen_main_index()
    packages = list_packages("public/")
    for package in packages:
        gen_package_index("public/"+package[0])
