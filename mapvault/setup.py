import setuptools

setuptools.setup(
    name="mapvault",
    version="0.2",
    author="k0rventen",
    description="A simpler interface to vault for retreiving secrets from a kv1 backend, superseeding hvac",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    install_requires=["hvac","maplog"]
)
