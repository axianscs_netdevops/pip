"""
vault module
"""
import os
import sys
import hvac
import requests
import maplog



class Vault():
    """A Vault object that represents a connection to a vault endpoint.

    It should be used as a module to authenticate to vault & retrieve secrets.


    >>> import mapvault
    >>> v = mapvault.Vault()
    >>> if v.authenticate():
    >>>     secrets = v.retrieve_services_credentials()
    >>> print(secrets)
    """

    def __init__(self, host=None, port=None, path=None, role=None):
        self.logger = maplog.logger("vault")
        self.host = os.getenv("VAULT_SERVICE_HOST", host)
        self.port = os.getenv("VAULT_SERVICE_PORT", port)
        self.client = None

        if self.host is None or self.port is None:
            self.host = "vault"
            self.port = "8200"

        # Adjust secrets path is modified by the user
        self.path = "secrets/"
        if path is not None:
            if path[-1] != "/":
                path += "/"
            self.path = path

        # Adjust Kubernetes role name if modified by user
        self.role = "microservice"
        if role is not None:
            self.role = role

    def authenticate(self):
        """Authenticate to vault using the k8 backend.

        It loads the service account token from /var.../token and tries to 
        authenticate to vault. 
        If any error rises, it will be printed.

        Returns:
            bool: True if authenticated, False otherwise
        """
        self.client = hvac.Client(url="http://"+self.host+":"+self.port)
        try:
            init = self.client.sys.is_initialized()
            sealed = self.client.sys.is_sealed()
        except requests.exceptions.ConnectionError as connError:
            if 'connection refused' in str(connError).lower():
                self.logger.error(
                    "Vault actively refused the connection, it might be sealed. Can't go further.")
            elif 'failure in name resolution' in str(connError).lower():
                self.logger.error(
                    "No response from host %s:%s, can't go further", self.host, self.port)
            else:
                self.logger.error(str(connError))
            return False

        if not init:
            self.logger.error("Vault is not initialized, can't go further..")
            return False
        if sealed:
            self.logger.error("Vault is sealed, can't go further..")
            return False

        try:
            with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as tkn:
                jwt = tkn.read()
            self.client.auth_kubernetes(self.role, jwt)
        except FileNotFoundError:
            self.logger.error(
                "No Service account token detected, can't go further.")
        except hvac.exceptions.VaultError as VaultError:
            self.logger.error(VaultError)
            return False
        if self.client.is_authenticated():
            return True
        return False

    def list_secrets(self, path):
        """Returns a list of secrets from a given path. It does walk subdirs.

        Args:
            path (str): the parent path to list secrets from. 
            Do not include the mount point of the secret engine.

        Returns:
            list: a list of str, representing the relative path of each secret.
        """
        try:
            secrets_list = self.client.list(self.path+path)
            if secrets_list is None:
                self.logger.error("No secrets detected for the path %s", path)
                return []
            secrets_list = secrets_list["data"]["keys"]

            secrets_list = [path+"/"+s for s in secrets_list]
            for secret in secrets_list:
                if secret[-1] == "/":
                    secrets_list.remove(secret)
                    secrets_list += self.list_secrets(secret[:-1])

        except Exception:
            self.logger.error("Unable to retrieve secrets.")
            return []
        return secrets_list

    def read_secret(self, path):
        """read a secret's data given its path

        Args:
            path (str): the secret path

        Returns:
            dict: k:v dict like {foo:bar} if the secret exists, None otherwise.
        """
        try:
            secret = self.client.read(self.path+path)["data"]
        except TypeError:
            self.logger.error("No secrets at this path: %s", path)
            return None
        return secret

    def retrieve_secrets(self, path):
        """given a root path, returns all secrets.

        Args:
            path (str): the root path to use

        Returns:
            dict: k:v dict where k is the secret path, v is a dict of values from that path
        """
        secrets = self.list_secrets(path)

        values = {}
        for secret in secrets:
            val = self.read_secret(secret)
            values[secret] = val
        return values

    def write_secret(self, path, secret):
        """create a new secret

        Args:
            path (str): the path of the new secret
            secret (dict): the dict of values to store as the secret

        Raises:
            NotImplementedError: method not impletmented yet
        """
        try:
            self.client.secrets.kv.v1.create_or_update_secret(
                mount_point=self.path,
                path=path,
                secret=secret,
                )
        except Exception:
            self.logger.error("Unable to write to the path %s", path)
            return False
        self.logger.info("New secret added to the path %s!", path)
        return True

    def delete_secret(self, path):
        """delete a secret at a given path.

        This requires the client ot be linked 
        to a policy that allows the 'delete' capability for the given path

        Args:
            path (str): the secret path to delete

        """
        try:
            self.client.secrets.kv.v1.delete_secret(
                path=path,
                )
        except Exception:
            self.logger.error("Unable to delete the path %s", path)
            return False
        self.logger.info("The secret at path %s was deleted!", path)
        return True

    def get_netbox_secrets(self):
        """returns the token for netbox auth

        Returns:
            dict: the secret for netbox, containing the token 
        """
        return self.read_secret("services/netbox/m2m")

    def get_minio_secrets(self):
        """returns the access and secret keys for minio

        Returns:
            dict: secret for minio
        """
        return self.read_secret("services/minio/m2m")

    def get_influx_secrets(self):
        """returns the user, pass and db for influx

        Returns:
            dict: secret for influx
        """
        return self.read_secret("services/influx/m2m")

    def get_grafana_secrets(self):
        """returns the user:pass for grafana

        Returns:
            dict: secret for grafana
        """
        return self.read_secret("services/grafana/m2m")


if __name__ == "__main__":
    v = Vault()
    if not v.authenticate():
        print("could not log into the vault!")
        sys.exit()

    print("authenticated!")
    print(v.get_netbox_secrets())
    print(v.get_minio_secrets())
    print(v.get_influx_secrets())
    print(v.get_netbox_secrets())
    print(v.get_grafana_secrets())
