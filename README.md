# pip

a pip repo for useful libraries

# What's that
This is a repository that hosts useful libraries that might be used in various microservices.

The CICD pipeline will generate the wheels for each package, and create the necessary files for the pip repo.

The resulting artifacts will be hosted on GitLab Pages, meaning we'll be able to `pip install` these libraries.

# Currently available modules :

- [maplib](#maplib), the gloabal wrapper
- [mapvault](#mapvault), high-level interface to vault
- [maplog](#maplog), formatted, centralised logs with sentry integration
- [maploki](#maploki), simple interface to push to loki
- [mapnetbox](#mapnetbox), a easier interface to netbox

__To avoid breaking the entire pip repo (which in turn can break the pipeline of all other projects using it)__
__Please follow the following steps to add a new module :__

- Clone the master branch
- Add your code under a new folder
- Add a quick user doc to the main Readme, below Available Modules
- Push your library to a new branch
- If all pipelines are OK (score & build), only then make a merge request.

# Documentation on available modules

## maplib

`maplib` is a global wrapper for every map services. It supports autoconfiguration, meaning you don't need to configure each client.

`pip3 install --extra-index-url https://axianscs_netdevops.gitlab.io/pip/ maplib`

the `maplib` module is composed of several sub-modules, each specific to a service. You'll find below a quick how-to on every sub-module : 

### maplib settings

If you need privileged access to Vault / the cluster, you'll need to configure maplib. __This implies that the serviceAccount tied to your pod is a privileged one__.


```python
import maplib

# i want to be privileged 
maplib.privileged()
```

### maplog

A pre-configured and pre-formatted logger. It ease logging and allow aggreagation later on.

```python
import maplib

logger = maplib.Logger("myapp")
logger.info("hello")
```


### mapinflux

A pre-configured influxDB client with a few simple functions to help creating points

```python
import maplib

i = maplib.Influx()
p = i.create_metric("ping",{"ip":"10.0.1.2"},{"avg":0.027639,"min":0.01373,"max":0.28983})
i.push(p)
```

### maploki

A simple pusher for Loki logs

```python
import maplib

l = maplib.Loki("myapp")
l.info("a line pushed to loki")
l.error("an error line pushed to loki")
```

### mapminio

A preconfigured, simpler minio client !

```python
import maplib

m = maplib.Minio()
m.create_bucket("files")
m.upload_file("./file.txt","files","file.txt")
```

### mapnetbox

a simpler pynetbox

```python
import maplib

n = maplib.Netbox()
t = n.get_hosts_with_tag("ping")
```
### mapvault

a preconfigured hvac client.

```python
import maplib

v = maplib.Vault()
v.read_secret("ssh/generic")
```

# Deprecated modules

These modules are no longer maintained, as we've moved to maplib.
They will still be there to not break backwards compatibility, 
but update to `maplib` ASAP.

## maplog

A pre-configured and pre-formatted logger. It ease logging and allow aggreagation later on.

`pip3 install --extra-index-url https://axianscs_netdevops.gitlab.io/pip/ maplog`

```python
import maplog

# Create the logger object
applog = maplog.logger("myapp")

# Use the logger functions instead of print()
applog.info("Hello")
applog.error("world")
```

## mapvault

A higher-level module for hvac, with kubernetes auth & secrets retrieval in a few lines.

`pip3 install --extra-index-url https://axianscs_netdevops.gitlab.io/pip/ mapvault`

By default, mapvault will use the env var `VAULT_SERVICE_HOST` and `VAULT_SERVICE_PORT`, as theses might be available in-cluster.
If theses env vars aren't set, it will fall back to `vault` and `8200`. 

```python
import mapvault

# Create the vault object
v = mapvault.Vault()

# Authenticate to vault
if v.authenticate():

    # if successful, retrieve all the secrets under a given path
    secrets = v.retrieve_secrets("microservices/mymicroservice")
    print(secrets)

    # all major services have their own function for retrieving their secrets.
    print(v.get_netbox_secrets())
    print(v.get_minio_secrets())
    print(v.get_influx_secrets())
    print(v.get_netbox_secrets())
    print(v.get_grafana_secrets())

```

## maploki

If you need to push data that is not purely metrics (int, float), use our loki log aggregator to do so. 

A quick note on how to properly log using loki : 

_It's not a index-based logging solution, meaning using 10 **labels for each log is useless** and will render loki slow as fuck. It's not Elastic search. Loki relies on fewer streams (labels) and huge **parallelized computation** when searching through logs. A good approach is to add labels directly to the line, in **logfmt** using `label=value`. For more, check this https://grafana.com/blog/2020/04/21/how-labels-in-loki-can-make-log-queries-faster-and-easier/._

Also, keep in mind that you must pass objects of type `str` to any maploki.Pusher(). If you retrieve a stdout from a subprocess, it might be in `bytes` and you will need to str.decode() before.
 
```python
import maploki

# create the loki pusher with our app as a tag
l = maploki.Pusher("myapp")

# push string to loki
l.info("This is some information about the target 192.168.1.1 !")

l.error('target=192.168.1.1 cmd=ps\n PID   USER     TIME  COMMAND\n    1 root      0:00 /bin/sh\n    6 root      0:00 sh\n   20 root      0:00 python3\n   23 root      0:00 ps\n')

l.warn("target=192.168.11:11 cmd=uptime\n up 11 days, 18:24, 4 users, load averages: 2.65 2.66 2.64")
```


## mapnetbox

```python
import mapnetbox
token = 'nbtoken'
nb = mapnetbox.netbox(token)
devices = nb.get_hosts_with_tag('dns')

```