"""
module that wraps pynetbox for an easier interface to netbox
"""
import pynetbox
import requests
from .mapvault import Vault
from .maplog import Logger

requests.urllib3.disable_warnings()


class Netbox():
    """netbox API object
    """

    def __init__(self, autoconfig=True, token="", url="netbox"):
        self.logger = Logger("netbox")

        if not url.startswith("http://") and not url.startswith("https://"):
            self.url = "http://"+url
        else:
            self.url = url

        if autoconfig:
            v = Vault()
            if v.authenticate():
                secret = v.get_netbox_secrets()
                self.token = secret["token"]
            else:
                self.logger.critical(
                    "autoconfig is on, but vault is unreacheable")
                exit()
        else:
            self.token = token
        self.nb = None

        # lambdas Getter
        self.get_full_dict = lambda x: dict(self.nb.dcim.devices.get(name=x))
        self.get_manufacturers = lambda: {x['name']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.manufacturers.all()]}
        self.get_models = lambda: {x['model']: x['id'] for x in [
            dict(x) for x in self.nb.dcim.device_types.all()]}
        self.get_ips = lambda: {x['address']: x['id'] for x in [
            dict(x) for x in self.nb.ipam.ip_addresses.all()]}
        self.get_interface_for_device = lambda device_name, interface_name: {x['name']: x for x in [
            dict(x) for x in self.nb.dcim.interfaces.get(device=device_name, name=interface_name)]}

        # lambda setters (create)
        self.create_manufacturer = lambda manufacturer_name: dict(self.nb.dcim.manufacturers.get(
            name=self.nb.dcim.manufacturers.create(name=manufacturer_name, slug=manufacturer_name)))['id']

        # autoconnect
        self.nb = pynetbox.api(self.url, self.token)
        if "https" in self.url:
            self.nb.http_session.verify = False

        # test
        try:
            self.nb.dcim.devices.all()
        except Exception as error:
            self.logger.error("could not connect to netbox : {}".format(error))

    def retrieve_hosts_with_tags(self, tag):
        """get the infos for each device in netbox who has a given tag

        Args:
            tag (str): the tag to filter devices

        Returns:
            dict: a dict of devices of type {'devicename':{'ip':"xx","tags":[]}}
        """
        hosts = {}
        for dev in self.nb.dcim.devices.all():
            dev_dict = dict(dev)
            if tag in dev_dict['tags']:
                try:
                    hosts[dev] = {'name': dev, 'ip': dev_dict['primary_ip']["address"].split(
                        "/")[0], "manufacturer": dev_dict["device_type"]['manufacturer']['slug'], 'tags': dev_dict['tags'], 'parsed_tags': parse_tags(dev_dict['tags'])}
                except Exception as error:
                    print(error)
        return hosts

    def get_hosts_with_tag(self, tag):
        """return hosts if one of its tag contains a given string

        Args:
            tag (str): the tag to filter devices with

        Returns:
            dict: a {k:{}} dict of devices, a device being a dict
        """
        hosts = {}
        for dev in self.nb.dcim.devices.all():
            dev_dict = dict(dev)
            if len([t for t in dev_dict['tags'] if tag in t]) != 0:
                try:
                    hosts[dev] = {'name': dev, 'ip': dev_dict['primary_ip']["address"].split(
                        "/")[0], "manufacturer": dev_dict["device_type"]['manufacturer']['slug'], 'tags': dev_dict['tags'], 'parsed_tags': parse_tags_to_list(dev_dict['tags'])}
                except Exception as error:
                    print(error)
        return hosts

    def find_device_by_name(self, hostname):
        """Find given ressource with specified name
        """
        try:
            list_target = self.nb.dcim.devices.filter(name=hostname)
        except:
            print("No targets found!")
            list_target = None
        return list_target

def parse_tags_to_list(tags_list):
    """parses tags from devices intelligently, by appending same keys as list

    Args:
        tags_list (list): the list of tags from netbox

    Returns:
        dict: a dict {k:[v]} for parsed tags
    """
    tags = {}
    for tag, val in [tuple(tag.split(":", 1)) for tag in [t for t in tags_list if ":" in t]]:
        if tag not in tags:
            tags[tag] = [val]
        else:
            tags[tag].append(val)

    return tags


def parse_tags(list_of_tags):
    """parse the tags from netbox by splitting by :

    Args:
        list_of_tags (list): the list of tags from netbox

    Returns:
        dict: a k:v dict of the tags splitted by : 
    """
    return {k: v for (k, v) in [tuple(tag.split(":", 1)) for tag in [t for t in list_of_tags if ":" in t]]}


if __name__ == "__main__":
    # connect
    nb = Netbox()  # for an automated login
    targets = nb.retrieve_hosts_with_tags("ping")
    print(targets)
