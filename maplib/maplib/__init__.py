"""
global wrapper for every service of MAP. 
"""

from .maplog import Logger
from .maploki import Loki
from .mapminio import Minio
from .mapnetbox import Netbox
from .mapvault import Vault,privileged
from .mapinflux import Influx
