"""wrapper for influx
"""
from influxdb import InfluxDBClient
from .mapvault import Vault
from .maplog import Logger

class Influx(InfluxDBClient):
    """A overwrite of the influx client that supports autoconfiguration using vault
    """
    def __init__(self,autoconfig=True,**kwargs):
        """init with either autoconfig to true (default), or just pass the arguments as you would on a regular InfluxDBClient instance

        Args:
            autoconfig (bool, optional): triggers autoconfig in a map env. Defaults to True.
        """
        self.logger = Logger("influx")
        if autoconfig:
            self.logger.debug("configuring automagically !")
            v = Vault()
            if v.authenticate():
                secret = v.get_influx_secrets()
                super().__init__("influx", 8086, secret["user"], secret["password"], secret["db"])
            else:
                self.logger.critical(
                    "Autoconfig is on but vault is not accessible, exiting !")
                exit()
        else:
            self.logger.debug("configuring through args..")
            super().__init__("influx", 8086, **kwargs)

    def push(self, point_or_points):
        """push a point or list of points to influx

        Args:
            point_or_points (dict or list of dicts): the points to push 
        """
        if isinstance(point_or_points, dict):
            self.write_points([point_or_points])
        elif isinstance(point_or_points, list):
            self.write_points(point_or_points)
        else:
            self.logger.warning("argument should be a point (dict) or list of points (list), was {}".format(
                type(point_or_points)))
