"""A simple grafana - loki logger lib
"""
import time
import requests
from .maplog import Logger


class Loki():
    """a logger for pushing messages to a loki instance
    """

    def __init__(self, app, host='loki', port=3100, proto="http"):
        self.logger = Logger("loki")
        self.host = host
        self.port = port
        self.proto = proto
        self.stream = {"app": app}

        # autocheck
        self.check_loki()

    def check_loki(self):
        """Check if the loki instance is reachable and running.
        As we don't want to break the upper app using this module, 
        just log the error and pass. 
        """
        check = requests.get(self.proto+'://'+self.host +
                             ':'+str(self.port)+'/ready')
        if check.status_code != 200:
            self.logger.error("Loki is not available")

    def push(self, message, additional_stream={"log_level": "INFO"}):
        """given a string as a argument, push that line to loki

        Args:
            message (str): the message to push to loki
            additional_stream (dict, optional): optionnal tags for indexing. Defaults to {}.

        """
        headers = {'Content-Type': 'application/json'}
        now = str(time.time_ns())
        stream_infos = self.stream
        stream_infos.update(additional_stream)
        data = {"streams": [
            {"stream": stream_infos, "values": [[now, message]]}]}
        response = requests.post(self.proto+'://'+self.host+':' +
                                 str(self.port)+'/loki/api/v1/push', headers=headers, json=data)
        if response.status_code != 204:
            self.logger.error("Could not push line to loki.")

    def info(self, message):
        """wrapper that pre-include a INFO tag

        Args:
            message {str} : log line to push
        """
        self.push(message, {"log_level": "INFO"})

    def warn(self, message):
        """wrapper that pre-include a WARNING tag

        Args:
            message {str} : log line to push
        """
        self.push(message, {"log_level": "WARNING"})

    def error(self, message):
        """wrapper that pre-include a ERROR tag

        Args:
            message {str} : log line to push
        """
        self.push(message, {"log_level": "ERROR"})


if __name__ == "__main__":
    loki = Loki("myapp")
    loki.info("maploki, info !")
    loki.error("maploki error.")
