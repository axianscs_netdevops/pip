import setuptools

setuptools.setup(
    name="maplib",
    version="0.8.6",
    author="k0rventen",
    description="a wrapper for map services",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    install_requires=["requests","sentry-sdk","hvac","pynetbox","minio",'influxdb']
)
