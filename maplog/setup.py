import setuptools

setuptools.setup(
    name="maplog",
    version="0.2",
    author="k0rventen",
    description="better log, better life",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    install_requires=["sentry-sdk==0.16.2"]
)
