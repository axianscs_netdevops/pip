"""
tests for the maplogger module
"""
import maplog

applog = maplog.logger("testing", "INFO")

def test_log(caplog):
    """test if the logs are correctly outputing to stderr

    Args:
        caplog (pytest.caplog): the cap for logging
    """
    applog.info("this is info")
    assert "this is info" in caplog.text
    assert "INFO" in caplog.text
    assert "testing" in caplog.text

def test_nolog(caplog):
    """test if the level defined by the logger is respected

    Args:
        caplog (pytest.caplog): the cap for logging
    """
    applog.debug("this should not be")
    assert "should" not in caplog.text
