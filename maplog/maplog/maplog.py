"""
a pre-configured logger.
Better log, better life.
"""
import logging
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration

def logger(name='app', level='INFO',sentry=False):
    """Creates a logger object that can be used to format log output
    It also sets up sentry error reporting for levels above INFO.
    
    Args:
        name (str, optional): the name of the app using the module. 
        If using a per-target logger, set the name to app_name-target-ip. Defaults to 'app'.
        level (str, optional): Level of the messages to log. 
        Can be DEBUG,INFO,WARNING,ERROR,CRITICAL. Defaults to 'INFO'.

    Returns:
        logging.Logger: A logger object
    """
    # If enabled, setup sentry error uploading
    if sentry:
        try:
            sentry_logging = LoggingIntegration(
                level=logging.WARNING,      
                event_level=logging.ERROR  
            )
            sentry_sdk.init('https://ed68fc8c39d541bfb4f57fda17ce615a@o403153.ingest.sentry.io/5265339',integrations=[sentry_logging])
        except:
            pass

    # Create a custom logger
    log = logging.getLogger(name)

    # add a streamhandler for stdout
    stdout_handler = logging.StreamHandler()

    # add the default debug level
    log.setLevel(level)

    # Format it
    stdout_format = logging.Formatter('%(asctime)s :: %(name)s :: %(levelname)s :: %(message)s', "%Y-%m-%d %H:%M:%S")
    stdout_handler.setFormatter(stdout_format)
    log.addHandler(stdout_handler)

    # return the logger object
    return log

if __name__ == "__main__":
    # quick example
    mylog = logger("testapp")

    mylog.debug("Debug time !")
    mylog.info("Just a quick note")
    mylog.warning("We're reaching a limit..")
    mylog.error("Something's definitely not right")
    mylog.critical("If we're here, that's a problem.")

    # How to handle exceptions nicely with logging
    try:
        a = 1 / 0 # Throws a ZeroDivisionError
    except ZeroDivisionError:
        mylog.exception("How about that. You can't divide by zero.")
