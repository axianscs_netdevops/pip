import setuptools

setuptools.setup(
    name="mapfoo",
    version="0.1",
    author="k0rventen",
    description="foo,bar",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ]
)
